package com.cs.myapplication.ex3

class Teacher(override var name: String) : User(name) {

    var students = mutableListOf<Student>()

    fun listStudents() : String{
        var list = ""
        for(i in students.indices) {
            list += students[i].name + ", "
        }
        return list
    }

    fun addStudent(studentToAdd: Student) {
        students.add(studentToAdd)
    }

    fun deleteStudent(studentToDelete: Student) {
        if(students.indexOf(studentToDelete) != -1) {
            students.remove(studentToDelete)
        } else {
            println("Error: Student does not exist")
        }
    }

    fun updateStudent(oldName: String, newName: String) {
        students.forEach{
            if(it.name == oldName) {
                it.name = newName
            }
        }
    }

}