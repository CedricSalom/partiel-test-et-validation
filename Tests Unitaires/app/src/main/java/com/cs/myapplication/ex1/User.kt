package com.cs.myapplication.ex1

class User(var id: Int, var name: String, var age: Int){

    fun addUser(tab: MutableList<User>, userToAdd: User) {
        tab.add(userToAdd)
    }

    fun deleteUser(tab: MutableList<User>, userToDelete: User) {
        if(tab.indexOf(userToDelete) != -1) {
            tab.remove(userToDelete)
        } else {
            println("Error: User does not exist")
        }
    }

    fun updateUser(tab: MutableList<User>, idUser: Int, newName: String, newAge: Int) {
        tab[idUser].name = newName
        tab[idUser].age = newAge
    }

}