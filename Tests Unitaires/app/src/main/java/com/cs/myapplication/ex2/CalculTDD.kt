package com.cs.myapplication.ex2

class CalculTDD {
    fun add(a: Int, b: Int) : Int{
        return a+b
    }

    fun sub(a: Int, b: Int) : Int{
        return a-b
    }

    fun div(a: Int, b: Int) : Int{
        return a/b
    }

    fun multi(a: Int, b: Int) : Int{
        return a*b
    }
}