package com.cs.myapplication

import com.cs.myapplication.ex3.Student
import com.cs.myapplication.ex3.Teacher
import org.hamcrest.core.IsEqual.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.junit.Assert.*

class TestEx3 {

    private var teacher = Teacher("Eric")


    @Test
    fun addStudentTest() {
        teacher.addStudent(Student("Paul"))

        assertThat(teacher.students.size, equalTo(1))
    }


    @Test
    fun listStudentsTest() {
        teacher.addStudent(Student("Arnaud"))

        assertEquals("Arnaud, ", teacher.listStudents())
    }

    @Test
    fun deleteStudentTest() {
        val s = Student("Paul")
        teacher.addStudent(s)
        teacher.deleteStudent(s)

        assertEquals(0, teacher.students.size)
    }

}