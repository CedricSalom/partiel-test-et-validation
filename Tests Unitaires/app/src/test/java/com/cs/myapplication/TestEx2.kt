package com.cs.myapplication

import com.cs.myapplication.ex2.CalculTDD

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.Test

class TestEx2 {

    var c = CalculTDD()

    @Test
    fun testCalculAdd() {
        assertThat(c.add(2, 3), equalTo(5))
    }

    @Test
    fun testCalculSub() {
        assertThat(c.sub(7, 3), equalTo(4))
    }

    @Test
    fun testCalculDiv() {
        assertThat(c.div(12, 4), equalTo(3))
    }

    @Test
    fun testCalculMultiple() {
        assertThat(c.multi(8, 4), equalTo(32))
    }

}