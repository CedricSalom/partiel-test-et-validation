package com.cs.myapplication

import com.cs.myapplication.ex1.User
import org.junit.Test
import org.junit.Assert.*

class TestEx1 {

    var listUser = mutableListOf<User>()
    private val user = User(0, "John", 20)

    @Test
    fun addUserTest() {
        user.addUser(listUser, user)

        assertEquals(1, listUser.size)
    }

    @Test
    fun deleteUserTest() {
        user.deleteUser(listUser, user)

        assertEquals(0, listUser.size)
    }

    @Test
    fun updateUserTest() {
        user.addUser(listUser, user)
        user.updateUser(listUser, 0, "Sam", 35)

        assertEquals("Sam", listUser[0].name)
        assertEquals(35, listUser[0].age)
    }
}